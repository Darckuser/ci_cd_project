import os
import pytest
from src.main import convert_currency

def test_convert_rub_to_usd_with_master_rate():
    os.environ["CURRENCY_RATE"] = "0.015"  # 1 USD = 66.67 RUB (master rate)
    os.environ["AMOUNT"] = "150"  # 150 RUB
    os.environ["CONVERSION_DIRECTION"] = "rub_to_usd"

    result = convert_currency(float(os.environ["AMOUNT"]), float(os.environ["CURRENCY_RATE"]),
                              os.environ["CONVERSION_DIRECTION"])
    assert result == 2.25  # Expected: 150 RUB = 2.25 USD

def test_convert_usd_to_rub_with_master_rate():
    os.environ["CURRENCY_RATE"] = "0.015"  # 1 USD = 66.67 RUB (master rate)
    os.environ["AMOUNT"] = "3"  # 3 USD
    os.environ["CONVERSION_DIRECTION"] = "usd_to_rub"

    result = convert_currency(float(os.environ["AMOUNT"]), float(os.environ["CURRENCY_RATE"]),
                              os.environ["CONVERSION_DIRECTION"])
    assert result == 200  # Expected: 3 USD = 200 RUB

def test_convert_rub_to_usd_with_master_custom_rate():
    os.environ["CURRENCY_RATE"] = "0.02"  # 1 USD = 50 RUB (custom rate for master)
    os.environ["AMOUNT"] = "100"  # 100 RUB
    os.environ["CONVERSION_DIRECTION"] = "rub_to_usd"

    result = convert_currency(float(os.environ["AMOUNT"]), float(os.environ["CURRENCY_RATE"]),
                              os.environ["CONVERSION_DIRECTION"])
    assert result == 2.0  # Expected: 100 RUB = 2.0 USD

def test_convert_usd_to_rub_with_master_custom_rate():
    os.environ["CURRENCY_RATE"] = "0.02"  # 1 USD = 50 RUB (custom rate for master)
    os.environ["AMOUNT"] = "2.5"  # 2.5 USD
    os.environ["CONVERSION_DIRECTION"] = "usd_to_rub"

    result = convert_currency(float(os.environ["AMOUNT"]), float(os.environ["CURRENCY_RATE"]),
                              os.environ["CONVERSION_DIRECTION"])
    assert result == 125  # Expected: 2.5 USD = 125 RUB
