import os
import pytest
from src.main import convert_currency


def test_convert_rub_to_usd_with_default_rate():
    os.environ["CURRENCY_RATE"] = "0.01"
    os.environ["AMOUNT"] = "100"  # 100 RUB
    os.environ["CONVERSION_DIRECTION"] = "rub_to_usd"

    result = convert_currency(float(os.environ["AMOUNT"]), float(os.environ["CURRENCY_RATE"]),
                              os.environ["CONVERSION_DIRECTION"])
    assert result == 1.0  # Expected: 100 RUB = 1.0 USD


def test_convert_usd_to_rub_with_default_rate():
    os.environ["CURRENCY_RATE"] = "0.01"
    os.environ["AMOUNT"] = "1.4"  # 1.4 USD
    os.environ["CONVERSION_DIRECTION"] = "usd_to_rub"

    result = convert_currency(float(os.environ["AMOUNT"]), float(os.environ["CURRENCY_RATE"]),
                              os.environ["CONVERSION_DIRECTION"])
    assert result == 140.0  # Expected: 1.4 USD = 140 RUB


def test_convert_rub_to_usd_with_custom_rate():
    os.environ["CURRENCY_RATE"] = "0.02"  # 1 USD = 50 RUB
    os.environ["AMOUNT"] = "100"  # 100 RUB
    os.environ["CONVERSION_DIRECTION"] = "rub_to_usd"

    result = convert_currency(float(os.environ["AMOUNT"]), float(os.environ["CURRENCY_RATE"]),
                              os.environ["CONVERSION_DIRECTION"])
    assert result == 2.0  # Expected: 100 RUB = 2.0 USD


def test_convert_usd_to_rub_with_custom_rate():
    os.environ["CURRENCY_RATE"] = "0.02"  # 1 USD = 50 RUB
    os.environ["AMOUNT"] = "2"  # 2 USD
    os.environ["CONVERSION_DIRECTION"] = "usd_to_rub"

    result = convert_currency(float(os.environ["AMOUNT"]), float(os.environ["CURRENCY_RATE"]),
                              os.environ["CONVERSION_DIRECTION"])
    assert result == 100  # Expected: 2 USD = 100 RUB
