import os


def convert_currency(amount, rate, direction):
    if direction == "rub_to_usd":
        return amount * rate
    elif direction == "usd_to_rub":
        return amount / rate
    else:
        raise ValueError("Недопустимое направление конвертации")

def print_all_environment_variables():
    for key, value in os.environ.items():
        print(f"{key} = {value}")

if __name__ == "__main__":
    print_all_environment_variables()
    currency_rate = float(os.environ.get("CURRENCY_RATE", 0.014))  # Дефолтный курс 1 USD = 71 RUB
    amount = float(os.environ.get("AMOUNT", 0))  # Считываем сумму из переменной окружения, с дефолтным значением 0
    conversion_direction = os.environ.get("CONVERSION_DIRECTION",
                                          "rub_to_usd")  # Направление конвертации по умолчанию - из рублей в доллары

    converted_amount = convert_currency(amount, currency_rate, conversion_direction)
    if conversion_direction == "rub_to_usd":
        print(f"Конвертированная сумма: {converted_amount} USD")
    elif conversion_direction == "usd_to_rub":
        print(f"Конвертированная сумма: {converted_amount} RUB")
